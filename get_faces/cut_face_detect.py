# -*- coding:utf-8 -*-

import cv2, os
import numpy as np

# 対象のディレクトリ名
target_dirname = 'linus-torvalds'
# 先ほど集めてきた画像データのあるディレクトリ
# input_data_path = '{}/images/' + str(target_dirname) + '/'.format(os.getcwd())
input_data_path = os.getcwd() + '/images/' + str(target_dirname) + '/'
# 切り抜いた画像の保存先ディレクトリ(予めディレクトリを作っておいてください)
# save_path = '{}/cutted_images/' + str(target_dirname) + '/'.format(os.getcwd())
save_path = os.getcwd() + '/cutted_images/' + str(target_dirname) + '/'
# OpenCVのデフォルトの分類器のpath。(https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xmlのファイルを使う) 非推奨
# cascadeファイルは公式サイトのものを利用する　https://opencv.org/ release > source で最新zipをDLする。
cascade_path = '/Users/maruku-dev/Develop/anaconda/cascades/haarcascade_frontalface_default.xml'
faceCascade = cv2.CascadeClassifier(cascade_path)

# 収集した画像の枚数(任意で変更)
image_count = 67
# 顔検知に成功した数(デフォルトで0を指定)
face_detect_count = 0

# 集めた画像データから顔が検知されたら、切り取り、保存する。
for i in range(image_count):
  file_path = input_data_path + str(i + 1).zfill(4) + '.jpg'
  print('path = : %s' % input_data_path)
  print('i = : %s' % i)
  # ファイルが存在する場合のみ
  if os.path.isfile(file_path):
    # 画像を読み込み
    #img = cv2.imread(file_path, cv2.IMREAD_COLOR)
    img = cv2.imread(file_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #物体認識（顔認識）の実行
    #scaleFactor – 各画像スケールにおける縮小量を表します
    #minNeighbors – 物体候補となる矩形は，最低でもこの数だけの近傍矩形を含む必要があります
    #minSize – 物体が取り得る最小サイズ．これよりも小さい物体は無視されます
    face = faceCascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=3)

    if len(face) > 0:
      for rect in face:
        x = rect[0]
        y = rect[1]
        w = rect[2]
        h = rect[3]

        img = img[y:y+h, x:x+w]
        # imgをリサイズ
        hight = img.shape[0]
        width = img.shape[1]
        if hight > 100 and width > 100:
          img = cv2.resize(img,(100,100))
          cv2.imwrite(save_path + 'cutted_' + str(target_dirname) + str(face_detect_count) + '.jpg', img)
          face_detect_count = face_detect_count + 1
    else:
      print('Image: %s :NoFace' % str(i))
