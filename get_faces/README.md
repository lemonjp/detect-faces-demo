## Bing 画像収集

### 事前準備

以下のサイトを参考にBeingのAPIキーを取得しておいてください。

[Bingの画像検索APIを使って画像を大量に収集する](https://qiita.com/ysdyt/items/49e99416079546b65dfc)

### APIキーをセット

以下ファイルにAPIキーをセットします。

`get_images_via_bing.py`

```python
# http リクエストヘッダ
kHeaders = { 'Ocp-Apim-Subscription-Key': '{YOUR-API-KEY}' }
```

### 実行時のﾊﾟﾗﾒｰﾀ

* image_count:１回のBing API呼出で取得する画像ファイル数
* call_count:1回のプログラム実行でのBing API呼出回数(image_count×call_countで総取得画像数)
* off_set_start:途中呼出時のどこから呼び出すか
* output_path:画像出力ディレクトリ
* query:検索語句


### 実行例

「linus-torvalds」という検索語句で、100件を1回取得します。

参考：[リーナス・トーバルズ](https://ja.wikipedia.org/wiki/%E3%83%AA%E3%83%BC%E3%83%8A%E3%82%B9%E3%83%BB%E3%83%88%E3%83%BC%E3%83%90%E3%83%AB%E3%82%BA)

* リナックスを作った人物です。


```bash

pip install requests

python get_images_via_bing.py --query "linus-torvalds" --call_count 1 --image_count 100

```

### 顔部分だけ抽出

顔部分だけを抽出して 100 x 100 にリサイズします。
cutted_imagesディレクトリの中にターゲット者の名前フォルダを先に作成してください。
また、imagesフォルダの中にも名前フォルダを作成し、そこに収集した顔画像を格納してください。

OpenCVのデフォルトの分類器ファイルが必要です。
(https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml)

```
curl -O https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_default.xml
```

公式サイトよりソースをダウンロードしてください。(Githubのものは余計なデータが入っている模様)

[OpenCV Release](https://opencv.org/releases/)


その他、各定数値を適宜変更してください。

```bash
pip install opencv-python

python cut_face_detect.py
```

## API利用せずに画像収集(推奨)

参考:
[API を叩かずに Google から画像収集をする](https://qiita.com/naz_/items/efc296ae1bf0e62f6704)

anacondaでpython3.7環境でターミナルを立ち上げて作業します。

```
pip install requests
pip install beautifulsoup4
pip install lxml
pip list
```

インストール確認

```
pip freeze | grep -e request -e lxml -e beautiful
```

リーナスの画像を100枚取得します。
* 顔でないファイルも取得されます。適宜削除してください。

```
cd get_faces/
python image_collector.py --target "linus-torvalds" --number 100 --directory "./images"
```




