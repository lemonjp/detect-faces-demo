detect-faces-demo
===============

機械学習を用いた顔認識システムのデモ（TensorFlowを使用）

### 開発環境

[Windows版AnacondaでTensorFlow環境構築](https://qiita.com/exy81/items/48314b968d9fad6170c8)

### 必要なパッケージ

AnacondaをインストールしてEnviroment を設定したら以下のパッケージをイントールしてください。

* Enviromentでのpythonは 3.6 を選択してください。

|パッケージ名|説明|
|---|---|
|pillow|画像を加工するときに必要|
|opencv|カメラから画像を読み込むときに必要|
|Matplotlib|可視化ツール|

*パッケージをインストールする前に `update index..`をクリックしてください。

### 参考

[TensorFlowチュートリアル - 熟練者のためのディープMNIST（翻訳）](https://qiita.com/KojiOhki/items/64a2ee54214b01a411c7)

### 実行例

1) 識別したい人別に以下のコマンドでデータ収集。

```bash
$ python get_face.py [name of person]
```

2) TensorFlowで学習

* 最新のTensorFlowは使えないのでv1.4あたりを利用する.

```bash
$ pip install tensorflow==1.14
$ pip install matplotlib
$ python train.py
```

3) Webカメラを起動し、顔認識デモ開始

```bash
$ python detect.py
```

4) Escキーで終了

### 識別したい実在の人物

顔認識で識別したい実在の人物（例：アメリカ大統領）の画像を収集したい場合は当レポジトリ内の以下を参照してください。

`/get_faces/README.MD`

